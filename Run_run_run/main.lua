--[[
    Collection
    Copyright (C) 2019  Soni Tourret

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
]]

function spawn_opponent()
    local opponent = {}

    opponent.x = 0
    opponent.y = 0
    opponent.channel = math.random(0, 2)

    return opponent
end

function spawn_coin()
    local coin = {}

    coin.x = 0
    coin.y = 0
    coin.channel = math.random(0, 2)

    return coin
end

function spawn_lifepoint()
    local lifepoint = {}

    lifepoint.x = 0
    lifepoint.y = 0
    lifepoint.channel = math.random(0, 2)

    return lifepoint
end

function love.load()
    math.randomseed(os.time())
    love.window.setTitle("Run run run !")

    player_x = 0
    player_y = 0
    player_channel = 1
    player_images = {
        love.graphics.newImage("player1.png"),
        love.graphics.newImage("player2.png")
    }
    player_width = player_images[1]:getWidth()
    player_height = player_images[1]:getHeight()
    player_anim_currentTime = 0
    player_anim_duration = 0.5
    player_lifes = 3
    player_coins = 0

    opponents_image = love.graphics.newImage("cactus.png")
    opponents_sound = love.audio.newSource("damage.ogg")
    opponents_speed = 100
    opponents = {}

    lifepoints_image = love.graphics.newImage("lifepoint.png")
    lifepoints_sound = love.audio.newSource("lifepoints.ogg")
    lifepoints_speed = 100
    lifepoints = {}

    coins_image = love.graphics.newImage("coin.png")
    coins_sound = love.audio.newSource("coin.ogg")
    coins_speed = 100
    coins = {}

    gameover = false
    gameover_image = love.graphics.newImage("gameover.png")
    gameover_music = love.audio.newSource("gameover.ogg")
    gameover_music:setLooping(true)

    move_sound = love.audio.newSource("move.ogg")

    music = love.audio.newSource("Ambiance.ogg")
    music:setLooping(true)

    table.insert(opponents, spawn_opponent())
    table.insert(coins, spawn_coin())
    table.insert(lifepoints, spawn_lifepoint())

    score = 0
end

function love.update(dt)
    if not gameover then
        -- player moving
        player_x = (love.graphics.getWidth() / 3) * player_channel + (((love.graphics.getWidth() / 3) - player_images[1]:getWidth()) / 2)
        player_y = love.graphics.getHeight() - player_height

        -- opponents moving
        for k, v in ipairs(opponents) do
            v.x = (love.graphics.getWidth() / 3) * v.channel + (((love.graphics.getWidth() / 3) - opponents_image:getWidth()) / 2)
            v.y = v.y + opponents_speed * dt
        end

        -- coins moving
        for k, v in ipairs(coins) do
            v.x = (love.graphics.getWidth() / 3) * v.channel + (((love.graphics.getWidth() / 3) - coins_image:getWidth()) / 2)
            v.y = v.y + coins_speed * dt
        end

        -- lifepoints moving
        for k, v in ipairs(lifepoints) do
            v.x = (love.graphics.getWidth() / 3) * v.channel + (((love.graphics.getWidth() / 3) - lifepoints_image:getWidth()) / 2)
            v.y = v.y + lifepoints_speed * dt
        end

        -- randomly add opponents
        coef = math.random(0, 10000)
        if coef < 30 then -- for 10000 of chance each tick
            table.insert(opponents, spawn_opponent())
        end
        -- delete opponents out of the screen
        for k, v in ipairs(opponents) do
            if v.x > love.graphics.getHeight() then
                table.remove(opponents, k)
            end
        end

        -- randomly add coins
        coin_coef = math.random(0, 10000)
        if coin_coef < 10 then
            table.insert(coins, spawn_coin())
        end
        -- delete coins out of the screen
        for k, v in ipairs(coins) do
            if v.x > love.graphics.getHeight() then
                table.remove(coins, k)
            end
        end

        -- randomly add lifepoints
        lifepoints_coef = math.random(0, 10000)
        if lifepoints_coef < 10 then
            table.insert(lifepoints, spawn_lifepoint())
        end
        -- delete lifepoints out of the screen
        for k, v in ipairs(lifepoints) do
            if v.x > love.graphics.getHeight() then
                table.remove(lifepoints, k)
            end
        end

        -- collisions management
        -- opponents collisions
        for k, v in ipairs(opponents) do
            if (opponents_image:getHeight() + v.y > player_y) and (v.y < player_height + player_y) and (v.channel == player_channel) then
                love.audio.play(opponents_sound)
                player_lifes = player_lifes - 1
                table.remove(opponents, k)
            end
        end
        -- coins collisions
        for k, v in ipairs(coins) do
            if (coins_image:getHeight() + v.y > player_y) and (v.y < player_height + player_y) and (v.channel == player_channel) then
                love.audio.play(coins_sound)
                player_coins = player_coins + 1
                table.remove(coins, k)
            end
        end
        -- lifepoints collisions
        for k, v in ipairs(lifepoints) do
            if (lifepoints_image:getHeight() + v.y > player_y) and (v.y < player_height + player_y) and (v.channel == player_channel) then
                love.audio.play(lifepoints_sound)
                player_lifes = player_lifes + 1
                table.remove(lifepoints, k)
            end
        end

        -- player animation
        player_anim_currentTime = player_anim_currentTime + dt
        if player_anim_currentTime >= player_anim_duration then
            player_anim_currentTime = player_anim_currentTime - player_anim_duration
        end

        -- score management
        score = score + 1 * (dt * 8)

        -- game over management
        if player_lifes < 1 then
            gameover = true
        end
    end
end

function love.draw()
    if not gameover then
        --[[-- draw lines
        love.graphics.line(love.graphics.getWidth() / 3, 0, love.graphics.getWidth() / 3, love.graphics.getHeight())
        love.graphics.line((love.graphics.getWidth() / 3) * 2, 0, (love.graphics.getWidth() / 3) * 2, love.graphics.getHeight())
        --]]

        -- music management
        music:play()

        -- draw player
        love.graphics.draw(player_images[math.floor(player_anim_currentTime / player_anim_duration * #player_images) + 1], player_x, player_y)

        -- draw opponents
        for k, v in ipairs(opponents) do
            love.graphics.draw(opponents_image, v.x, v.y)
        end

        -- draw coins
        for k, v in ipairs(coins) do
            love.graphics.draw(coins_image, v.x, v.y)
        end

        -- draw lifepoints
        for k, v in ipairs(lifepoints) do
            love.graphics.draw(lifepoints_image, v.x, v.y)
        end

        -- draw score
        love.graphics.setColor(128, 128, 128)
        love.graphics.rectangle("fill", 0, 0, 100, 25)
        love.graphics.setColor(255, 255, 255)
        love.graphics.print(math.floor(score), 5, 5)

        -- draw life points
        love.graphics.setColor(255, 255, 255)
        for i = 1, player_lifes, 1 do
            love.graphics.draw(lifepoints_image, love.graphics.getWidth() / 3 + (i * lifepoints_image:getWidth()), 0)
        end

        -- draw player's coins
        love.graphics.draw(coins_image, (love.graphics.getWidth() / 3) * 2, 0)
        love.graphics.print(player_coins, ((love.graphics.getWidth() / 3) * 2) + coins_image:getWidth() + 20, coins_image:getHeight() / 2)
    else
        music:stop()
        gameover_music:play()
        love.graphics.draw(gameover_image, 0, 0, 0, love.graphics.getWidth() / gameover_image:getWidth(), love.graphics.getHeight() / gameover_image:getHeight())
    end
end

function love.keypressed(key)
    if key == "left" then
        love.audio.play(move_sound)
        if player_channel <= 0 then
            player_channel = 2
        else
            player_channel = player_channel - 1
        end
    elseif key == "right" then
        love.audio.play(move_sound)
        if player_channel >= 2 then
            player_channel = 0
        else
            player_channel = player_channel + 1
        end
    elseif key == "f11" then
        love.window.setFullscreen(not love.window.getFullscreen())
    end
end
