--[[
    Collection
    Copyright (C) 2019  Soni Tourret

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
]]

function love.load()
	math.randomseed(os.time())
	love.window.setTitle("Cuboid")

	bw = 50
	bh = 50
	bx = 0
	by = 0
	
	colorsList = {
		{000,255,255}, -- aqua
		{000,000,000}, -- black
		{000,000,255}, -- blue
		{255,000,255}, -- fuchsia
		{000,128,000}, -- gree
		{128,128,128}, -- gray
		{000,255,000}, -- lime
		{128,000,000}, -- maroon
		{000,000,128}, -- navy
		{128,128,000}, -- olive
		{128,000,128}, -- purple
		{255,000,000}, -- red
		{192,192,192}, -- silver
		{000,128,128}, -- teal
		{255,255,255}, -- white
		{255,255,000}, -- yellow
	}
	colorsListPointer = 1
	
	actualCubeColorR = colorsList[colorsListPointer][1]
	actualCubeColorG = colorsList[colorsListPointer][2]
	actualCubeColorB = colorsList[colorsListPointer][3]
	
	staticCubes = {}
	
	-- toolbar
	toolbarX = 0
	toolbarY = 0
	toolbarW = love.graphics.getWidth()
	toolbarH = 50
	toolbarColor = {128, 128, 128}
	toolbarColorIndicatorW = 20
	toolbarColorIndicatorH = 20
	toolbarColorIndicatorPointerW = 20
	toolbarColorIndicatorPointerH = 5
	toolbarColorIndicatorPointerColor = {255, 255, 255}
	toolbarTextColor = {255, 255, 255}
	
	grid = true
	gridColor = {192, 192, 192}
	gridW = bw
	gridH = bh
end

function love.update(dt)
	local a, b = love.mouse.getPosition()
	if b >= toolbarH then
		bx, by = love.mouse.getPosition()
	end
end

function love.draw()
	-- static cubes
	for k, v in ipairs(staticCubes) do
		love.graphics.setColor(v[5], v[6], v[7])
		love.graphics.rectangle("fill", v[1], v[2], v[3], v[4])
	end
	
	-- actual cube
	love.graphics.setColor(actualCubeColorR, actualCubeColorG, actualCubeColorB)
	love.graphics.rectangle("fill", bx, by, bw, bh)
	
	-- grid
	if grid then
		gridDrawX = gridW
		gridDrawY = gridH
		while gridDrawX <= love.graphics.getWidth() do
			love.graphics.setColor(gridColor)
			love.graphics.line(gridDrawX, 0, gridDrawX, love.graphics.getHeight())
			love.graphics.line(0, gridDrawY, love.graphics.getWidth(), gridDrawY)
			gridDrawX = gridDrawX + gridW
			gridDrawY = gridDrawY + gridH
		end
	end
	
	-- toolbar
	love.graphics.setColor(toolbarColor[1], toolbarColor[2], toolbarColor[3])
	love.graphics.rectangle("fill", toolbarX, toolbarY, toolbarW, toolbarH)
	local drawX = 10
	local drawY = 10
	for k, v in ipairs(colorsList) do
		love.graphics.setColor(v[1], v[2], v[3])
		love.graphics.rectangle("fill", drawX, drawY, toolbarColorIndicatorW, toolbarColorIndicatorH)
		
		if (actualCubeColorR == v[1]) and (actualCubeColorG == v[2]) and (actualCubeColorB == v[3]) then
			love.graphics.setColor(toolbarColorIndicatorPointerColor[1], toolbarColorIndicatorPointerColor[2], toolbarColorIndicatorPointerColor[3])
			love.graphics.rectangle("fill", drawX, toolbarH - toolbarColorIndicatorPointerH, toolbarColorIndicatorPointerW, toolbarColorIndicatorPointerH)
		end
		
		drawX = drawX + toolbarColorIndicatorW
	end
	
	drawX = drawX + 5
	
	love.graphics.setColor(toolbarTextColor[1], toolbarTextColor[2], toolbarTextColor[3])
	love.graphics.print("Width = " .. bw, drawX, drawY)
	love.graphics.print("X, Y = " .. bx .. ", " .. by, drawX, drawY * 2)
	
	drawX = drawX + 102
	love.graphics.line(drawX, toolbarY + 5, drawX, toolbarH - 5)
	drawX = drawX + 4
	
	love.graphics.print("G : grid on/off", drawX, drawY)
	love.graphics.print("space : color switch", drawX, drawY * 2)
	love.graphics.print("enter : place cube", drawX, drawY * 3)
	
	drawX = drawX + 127
	love.graphics.line(drawX, toolbarY + 5, drawX, toolbarH - 5)
	drawX = drawX + 4
end

function love.keypressed(key)
	if key == "up" then
		bw = bw + 5
		bh = bh + 5
	elseif key == "down" then
		bw = bw - 5
		bh = bh - 5
	elseif key == "g" then
		grid = not grid
	end
end

function love.keyreleased(key)
	if key == "space" then
		if colorsListPointer < 16 then
			colorsListPointer = colorsListPointer + 1
		else
			colorsListPointer = 1
		end
		actualCubeColorR = colorsList[colorsListPointer][1]
		actualCubeColorG = colorsList[colorsListPointer][2]
		actualCubeColorB = colorsList[colorsListPointer][3]
	end
	if key == "return" then
		local a, b = love.mouse.getPosition()
		if b >= toolbarH then
			table.insert(staticCubes, {bx, by, bw, bh, actualCubeColorR, actualCubeColorG, actualCubeColorB})
		end
	end
end

function love.mousepressed(x, y, button, istouch, presses)
	if button == 1 then
		local a, b = love.mouse.getPosition()
		if b >= toolbarH then
			table.insert(staticCubes, {bx, by, bw, bh, actualCubeColorR, actualCubeColorG, actualCubeColorB})
		end
	elseif button == 2 then
		if colorsListPointer < 16 then
			colorsListPointer = colorsListPointer + 1
		else
			colorsListPointer = 1
		end
		actualCubeColorR = colorsList[colorsListPointer][1]
		actualCubeColorG = colorsList[colorsListPointer][2]
		actualCubeColorB = colorsList[colorsListPointer][3]
	end
end
-- uwu