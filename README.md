# Collection

Collection est un ensemble de petits projets en Lua avec Löve2D. Ils n'ont aucun lien entre eux, et sont très simples.

Vous pouvez utiliser librement le code de ces projets, tant que vous respectez la licence d'utilisation (GNU).

# Les différents projets

## Cuboid

Cuboid est une sorte d'application de dessin basé sur l'utilisation de cubes. Malheureusement, il est pour l'instant impossible de sauvegarder les travaux créés...

Répertoire : `/Cuboid`

## Follow me ma bruda

Ce minuscule projet est extrêmement simple : il présente simplement un cube qui suit les mouvements de la souris.

Répertoire : `/follow_me_ma_bruda`

## Run run run !

Ce projet présente un personnage qui court dans des couloirs. On peut changer le couloir avec les flèches directionnelles. Il doit éviter les cactus, mais peut utiliser les coeurs pour reprendre des points de vie.

Répertoire : `/Run_run_run`

Les sons présents dans le jeu (`coin.ogg`, `damage.ogg`, `lifepoints.ogg` et `move.ogg`) proviennent du logiciel RPG Maker VM. Les musiques présentes dans ce jeu (`Ambiance.ogg` et `gameover.ogg`) sont de ma composition, ainsi que les sprites (`cactus.png`, `coin.png`, `gameover.png`, `lifepoint.png`, `player1.png` et `player2.png`).