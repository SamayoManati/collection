--[[
    Collection
    Copyright (C) 2019  Soni Tourret

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
]]

function love.load()
    love.window.setTitle("Follow me ma bruda")

	mx, my = love.mouse.getPosition()
end

function love.update(dt)
	mx, my = love.mouse.getPosition()
end

function love.draw()
	love.graphics.rectangle("fill", mx - 25, my - 25, 50, 50)
end